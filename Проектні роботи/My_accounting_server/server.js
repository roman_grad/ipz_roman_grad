import http from 'http';
import decodeUriComponent from 'decode-uri-component';
import {
    login, 
    registration, 
    writeTime,
    writePaymentData,
    getPaymentData,
    accrual,
    addSchedule,
    getSchedule
} from './database.js';


const port = process.env.PORT || 3000;

http.createServer((request, response) => {
    let body = '';
    request.on('data', chunk => {
        body += chunk;
    });
    request.on('end', async () => {
        const func = body.split('=')[0];
        const data = decodeUriComponent(body.split('=')[1]).split('&');
        let resp = '';
        console.log(`Запит: ${func}`);
        if(!!data[0]) {
            console.log(data);
        }
        

        if(func === 'login') {
            resp = await login(data[0], data[1]);
        }
        if(func === 'registration') {
            resp = await registration(data[0], data[1], data[2], data[3], data[4], data[5], data[6], data[7], data[8], data[9]);
        }
        if(func === 'writeTime') {
            resp = await writeTime(data[0], data[1]);
        }
        if(func === 'writePaymentData') {
            resp = await writePaymentData(data[0], data[1], data[2], data[3]);
        }
        if(func === 'getPaymentData') {
            resp = await getPaymentData(data[0]);
        }
        if(func === 'accrual') {
            resp = await accrual(data[0], data[1], data[2], data[3]);
        }
        if(func === 'addSchedule') {
            resp = await addSchedule(data[0], data[1], data[2], data[3], data[4], data[5], data[6], data[7]);
        }
        if(func === 'getSchedule') {
            resp = await getSchedule(data[0]);
        }
        if(func === 'logout') {
            resp = 'true';
        }

        console.log(`Відповідь: ${resp}\n`);
        response.end(resp);
    });
}).listen(port, () => console.log(`Server started on port ${port}`));