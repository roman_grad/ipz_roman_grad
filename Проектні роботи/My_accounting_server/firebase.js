
import firebase from "firebase/compat/app";
import "firebase/compat/database";

const firebaseConfig = {
  aapiKey: "AIzaSyChoVf_Sr1QpddS0QCDVMKR_qPFPO3etyg",
  authDomain: "lucifer-b05f4.firebaseapp.com",
  databaseURL: "https://lucifer-b05f4-default-rtdb.europe-west1.firebasedatabase.app",
  projectId: "lucifer-b05f4",
  storageBucket: "lucifer-b05f4.appspot.com",
  messagingSenderId: "757050069717",
  appId: "1:757050069717:web:f57e09e981e6c481a2085c"
};

firebase.default.initializeApp(firebaseConfig);

export async function GET_DATA(path) {
  let response;
  await firebase.database().ref(path).get().then(data => {
      if(data.exists()) {
        response = data.val();
      } 
      else {
        response = false;
      }
  });
  return response;
}

export async function WRITE_DATA(updates) {
  await firebase.database().ref().update(updates);
}


