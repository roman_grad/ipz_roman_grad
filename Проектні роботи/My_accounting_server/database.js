import {GET_DATA, WRITE_DATA} from './firebase.js';


export async function login(login, password) {
    let message = '';
    const data = await GET_DATA(`Users/${login}`);
    if(data) {
        if(data.password === password) {
            message = 'true';
        }
        else {
            message = 'Пароль неправильний!';
        }
    } 
    else {
        message = 'Користувач з даним логіном відсутній!';
    }

    return message;
}

export async function registration(name, surname, lastname, city, age, posada, education, phone, login, password) {
    name = name[0].toUpperCase() + name.slice(1);
    surname = surname[0].toUpperCase() + surname.slice(1);
    lastname = lastname[0].toUpperCase() + lastname.slice(1);
    city = city[0].toUpperCase() + city.slice(1);
    posada = posada[0].toUpperCase() + posada.slice(1);
    education = education[0].toUpperCase() + education.slice(1);
    let message = '';
    if(await GET_DATA(`Users/${login}`)) {
        message = 'Користувач з даним логіном вже існує.';
    }
    else {
        let updates = {};
        updates[`Users/${login}/name`] = name;
        updates[`Users/${login}/surname`] = surname;
        updates[`Users/${login}/lastname`] = lastname;
        updates[`Users/${login}/city`] = city;
        updates[`Users/${login}/age`] = age;
        updates[`Users/${login}/posada`] = posada;
        updates[`Users/${login}/education`] = education;
        updates[`Users/${login}/phone`] = phone;
        updates[`Users/${login}/login`] = login;
        updates[`Users/${login}/password`] = password;

        await WRITE_DATA(updates);
        message = 'Реєстрація пройшла успішно!';
    }
    return message;
}

export async function writeTime(login, time) {
    let updates = {};
    const data = await GET_DATA(`Users/${login}/time`);
    const minutes = Number(time.split(':')[0]) * 60 + Number(time.split(':')[1]);
    if(data) {
        updates[`Users/${login}/time`] = Number(data) + minutes;
    } 
    else {
        if(minutes !== 0) {
            updates[`Users/${login}/time`] = minutes;
        }
    }
    let message = `Ви успішно відпрацювали ${time}.`;
    await WRITE_DATA(updates).catch(() => message = '');
    return message;
}


export async function writePaymentData(login, identificationСode, passportNumber, cardNumber) {
    let message = 'Дані звережено.';
    let updates = {};
    updates[`Users/${login}/identificationСode`] = identificationСode;
    updates[`Users/${login}/passportNumber`] = passportNumber;
    updates[`Users/${login}/cardNumber`] = cardNumber;
    await WRITE_DATA(updates).catch(() => message = 'Не вдалось зберегти дані.');
    return message;
}

export async function getPaymentData(login) {
    let response = '';
    const data = await GET_DATA(`Users/${login}`);
    if(data.identificationСode && data.passportNumber && data.cardNumber) {
        response = `${data.identificationСode}&${data.passportNumber}&${data.cardNumber}`;
    }
    return response;
}

export async function accrual(login, userName, userSurname, userPosada) {
    userName = userName[0].toUpperCase() + userName.slice(1);
    userSurname = userSurname[0].toUpperCase() + userSurname.slice(1);
    let response = '';
    const position = {
        'Директор': 100,
        'Заступник': 80,
        'Менеджер': 60,
        'Працівник': 40
    }
    const data = await GET_DATA(`Users/${login}`);
    if(data) {
        if(data.name != userName) {
            response = `Вказане ім'я невірне.`;
        }
        else if(data.surname != userSurname) {
            response = `Вказане прізвище невірне.`;
        }
        else if(data.posada != userPosada) {
            response = `Ваша посада не ${userPosada}.`;
        }
        else {
            response = `Час - ${parseInt(data.time / 60)} год\nСтавка - ${position[userPosada]} грн\nЗаробіток - ${parseInt(data.time / 60) * position[userPosada]} грн`;
        }
    }
    return response;
}

export async function addSchedule(login, mon, tues, wednes, thurs, fri, satur, sun) {
    let message = 'Розклад звережено.';
    let updates = {};
    updates[`Users/${login}/Schedule/mon`] = mon;
    updates[`Users/${login}/Schedule/tues`] = tues;
    updates[`Users/${login}/Schedule/wednes`] = wednes;
    updates[`Users/${login}/Schedule/thurs`] = thurs;
    updates[`Users/${login}/Schedule/fri`] = fri;
    updates[`Users/${login}/Schedule/satur`] = satur;
    updates[`Users/${login}/Schedule/sun`] = sun;
    await WRITE_DATA(updates).catch(() => message = 'Не вдалось зберегти розклад.');
    return message;
}

export async function getSchedule(login) {
    let response = '';
    const data = await GET_DATA(`Users/${login}/Schedule`);
    if(data) {
        Object.values(data).forEach(item => {
            if(item === 'Робочий') {
                response += '0&';
            }
            if(item === 'Вихідний') {
                response += '1&';
            }
        });
    }
    return response.slice(0, -1);
}
