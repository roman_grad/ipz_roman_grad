@startuml
:Checking the schedule of employers;
:Sending a request to the database of employees;
:Checking the working hours of employees;
:Sorting employees by working hours;
if (Is a new employee ?) then (Yes)
 :adding new employer;
else(No)
endif
:Display the sorted schedule of employees;
end
@enduml