﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Collections.Specialized;

namespace My_accounting
{
    public partial class Rozklsd : Form
    {
        public Rozklsd()
        {
            InitializeComponent();
            try
            {
                using (var webClient = new MyWebClient())
                {
                    var pars = new NameValueCollection();
                    pars.Add("getSchedule", Data.Login);
                    var response = webClient.UploadValues(Data.URL, pars);
                    string str = System.Text.Encoding.UTF8.GetString(response);
                    if(str != "")
                    {
                        var data = str.Split('&');
                        listBox1.SelectedIndex = Convert.ToInt32(data[1]);
                        listBox2.SelectedIndex = Convert.ToInt32(data[5]);
                        listBox3.SelectedIndex = Convert.ToInt32(data[6]);
                        listBox4.SelectedIndex = Convert.ToInt32(data[4]);
                        listBox5.SelectedIndex = Convert.ToInt32(data[0]);
                        listBox6.SelectedIndex = Convert.ToInt32(data[2]);
                        listBox7.SelectedIndex = Convert.ToInt32(data[3]);
                    }
                }
            }
            catch (WebException)
            {
                MessageBox.Show("Сервер не відповідає, спробуйте пізніше.");
            }

        }

        private void BackButton_Click(object sender, EventArgs e)
        {
            Menu form = new Menu();
            form.FormClosed += new FormClosedEventHandler(delegate { Close(); });
            form.Show();
            this.Hide();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex == -1 || listBox2.SelectedIndex == -1 || listBox3.SelectedIndex == -1 || listBox4.SelectedIndex == -1 || listBox5.SelectedIndex == -1 || listBox6.SelectedIndex == -1 || listBox7.SelectedIndex == -1)
            {
                MessageBox.Show("Заповність всі поля.", "Помилка",
                MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                int isWeekend = 0;
                string tmp = listBox1.Text + '&' + listBox2.Text + '&' + listBox3.Text + '&' + listBox4.Text + '&' + listBox5.Text + '&' + listBox6.Text + '&' + listBox7.Text;
                for(int i = 0; i < tmp.Split('&').Length; i++)
                {
                    if(tmp.Split('&')[i] == "Вихідний")
                    {
                        isWeekend += 1;
                    }
                }
                if(isWeekend > 2)
                {
                    MessageBox.Show("Кількість вихідних днів не може бути більше двох.", "Помилка",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    try
                    {
                        using (var webClient = new MyWebClient())
                        {
                            var pars = new NameValueCollection();
                            pars.Add("addSchedule", Data.Login + '&' + tmp);
                            var response = webClient.UploadValues(Data.URL, pars);
                            string str = System.Text.Encoding.UTF8.GetString(response);
                            MessageBox.Show(str);
                        }
                    }
                    catch (WebException)
                    {
                        MessageBox.Show("Сервер не відповідає, спробуйте пізніше.");
                    }
                }
            }
        }

        private void Rozklsd_Load(object sender, EventArgs e)
        {

        }
    }
}
