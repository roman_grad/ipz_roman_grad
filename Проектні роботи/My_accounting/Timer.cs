﻿using System;
using System.Diagnostics;
using System.Windows.Forms;
using System.Windows.Threading;
using System.Net;
using System.Collections.Specialized;

namespace My_accounting
{
    public partial class Timer : Form
    {
        DispatcherTimer dispatcherTimer = new DispatcherTimer();
        Stopwatch sw = new Stopwatch();
        string currentTime = string.Empty;
        

        public Timer()
        {
            InitializeComponent();
            dispatcherTimer.Tick += new EventHandler(dt_Tick);
            dispatcherTimer.Interval = new TimeSpan(0, 0, 0, 0, 1);
        }

        private void Timer_Load(object sender, EventArgs e)
        {

        }

        private void dt_Tick(object sender, EventArgs e)
        {
            if (sw.IsRunning)
            {
                TimeSpan ts = sw.Elapsed;
                currentTime = String.Format("{0:00}:{1:00}:{2:00}",
                ts.Hours, ts.Minutes, ts.Seconds);
                textBox1.Text = currentTime;
            }
        }

        private void startbtn_Click(object sender, EventArgs e)
        {
            sw.Start();
            dispatcherTimer.Start();
        }

        private void stopbtn_Click(object sender, EventArgs e)
        {
            if (sw.IsRunning)
            {
                sw.Stop();
            }
            textBox2.Text= currentTime ;
        }

        private void resetbtn_Click(object sender, EventArgs e)
        {
            try
            {
                using (var webClient = new MyWebClient())
                {
                    var pars = new NameValueCollection();
                    pars.Add("writeTime", Data.Login + '&' + textBox2.Text);
                    var response = webClient.UploadValues(Data.URL, pars);
                    string str = System.Text.Encoding.UTF8.GetString(response);
                    if (str != "")
                    {
                        MessageBox.Show(str);
                        Menu form = new Menu();
                        form.FormClosed += new FormClosedEventHandler(delegate { Close(); });
                        form.Show();
                        this.Hide();
                    }
                    else
                    {
                        MessageBox.Show("Сталася помилка, час не було записано.");
                    }
                }
            }
            catch (WebException)
            {
                MessageBox.Show("Сервер не відповідає, спробуйте пізніше.");
            }
            sw.Reset();
            textBox1.Text = "00:00:00";
        }

        private void Back_Click(object sender, EventArgs e)
        {
            Menu form = new Menu();
            form.FormClosed += new FormClosedEventHandler(delegate { Close(); });
            form.Show();
            this.Hide();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
