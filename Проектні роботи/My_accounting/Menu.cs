﻿using System;
using System.Windows.Forms;
using System.Windows.Threading;

namespace My_accounting
{
    public partial class Menu : Form
    {
        
        public Menu()
        {
            InitializeComponent();  
        }

        private void vuplata_Click(object sender, EventArgs e)
        {
            Vuplata form = new Vuplata();
            form.FormClosed += new FormClosedEventHandler(delegate { Close(); });
            form.Show();
            this.Hide();
        }

        private void rozklad_Click(object sender, EventArgs e)
        {
            Rozklsd form = new Rozklsd();
            form.FormClosed += new FormClosedEventHandler(delegate { Close(); });
            form.Show();
            this.Hide();
        }

        private void narahuvanna_Click(object sender, EventArgs e)
        {
            Narahuvanna form = new Narahuvanna();
            form.FormClosed += new FormClosedEventHandler(delegate { Close(); });
            form.Show();
            this.Hide();
        }

        private void Timer_Click(object sender, EventArgs e)
        {
            Timer form = new Timer();
            form.FormClosed += new FormClosedEventHandler(delegate { Close(); });
            form.Show();
            this.Hide();
        }

        private void Menu_Load(object sender, EventArgs e)
        {

        }
    }
}

