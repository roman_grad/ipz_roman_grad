﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.Net;
using System.Collections.Specialized;


namespace My_accounting
{
    public partial class Narahuvanna : Form
    {
        public Narahuvanna()
        {
            InitializeComponent();
        }

        private void back_button_Click(object sender, EventArgs e)
        {
            Menu form = new Menu();
            form.FormClosed += new FormClosedEventHandler(delegate { Close(); });
            form.Show();
            this.Hide();
        }

        private void OKButton_Click_1(object sender, EventArgs e)
        {
            if(Name_textBox.TextLength == 0 || Surname_textBox.TextLength == 0 || listBox2.SelectedIndex == -1)
            {
               MessageBox.Show("Заповність всі поля.", "Помилка",
               MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                try
                {
                    using (var webClient = new MyWebClient())
                    {
                        var pars = new NameValueCollection();
                        pars.Add("accrual", Data.Login + '&' + Name_textBox.Text + '&' + Surname_textBox.Text + '&' + listBox2.Text);
                        var response = webClient.UploadValues("http://localhost:3000", pars);
                        string str = System.Text.Encoding.UTF8.GetString(response);
                        if (str != "Вказане ім'я невірне." && str != "Вказане прізвище невірне." && str != "Ваша посада не " + listBox2.Text + '.')
                        {
                            var data = str.Split('\n');
                            listBox1.Items.Clear();
                            listBox1.Items.AddRange(data);
                        }
                        else
                        {
                            MessageBox.Show(str);
                        }
                    }
                }
                catch (WebException)
                {
                    MessageBox.Show("Сервер не відповідає, спробуйте пізніше.");
                }
            }
        }

        private void Narahuvanna_Load(object sender, EventArgs e)
        {

        }

        private void listBox2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
