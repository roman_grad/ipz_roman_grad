﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.Net;
using System.Collections.Specialized;

namespace My_accounting
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

        }

        private void Sing_up_Click(object sender, EventArgs e)
        {
            if (textlogin.TextLength == 0 || textpassword.TextLength == 0)
            {
                MessageBox.Show("Введіть дані для авторизації" ,"Помилка");
                
            }
            else
            {
                try
                {
                    using (var webClient = new MyWebClient())
                    {
                        var pars = new NameValueCollection();
                        pars.Add("login", textlogin.Text + '&' + textpassword.Text);
                        var response = webClient.UploadValues("http://localhost:3000/", pars);
                        string str = System.Text.Encoding.UTF8.GetString(response);
                        if (str == "true")
                        {
                            Data.Login = textlogin.Text;
                            Menu form = new Menu();
                            form.FormClosed += new FormClosedEventHandler(delegate { Close(); });
                            form.Show();
                            this.Hide();
                        }
                        else
                        {
                            MessageBox.Show(str);
                        }
                    }
                }
                catch (WebException)
                {
                    MessageBox.Show("Сервер не відповідає, спробуйте пізніше.");
                }
            }
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            registration form = new registration();
            form.Show();
            this.Hide();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
