﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.Net;
using System.Collections.Specialized;

namespace My_accounting
{
    public partial class registration : Form
    {
        
        public registration()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            if (name.TextLength == 0 || surname.TextLength == 0 || lastname.TextLength == 0 || city.TextLength == 0 || posada.TextLength == 0 || education.TextLength == 0 || phone.TextLength == 0 || login.TextLength == 0 || password.TextLength == 0)
            {
                MessageBox.Show("Заповність всі дані щоб зареєструватись.", "Помилка ",
                MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (Regex.IsMatch(name.Text, @"[^\u0400-\u04FF]"))
            {
                MessageBox.Show("Поле ім'я повинно містити тільки кирилицю.", "Помилка ",
                MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (Regex.IsMatch(surname.Text, @"[^\u0400-\u04FF]"))
            {
                MessageBox.Show("Поле прізвище повинно містити тільки кирилицю.", "Помилка ",
                MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (Regex.IsMatch(lastname.Text, @"[^\u0400-\u04FF]"))
            {
                MessageBox.Show("Поле по батькові повинно містити тільки кирилицю.", "Помилка ",
                MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (Regex.IsMatch(city.Text, @"[^\u0400-\u04FF]"))
            {
                MessageBox.Show("Поле місто повинно містити тільки кирилицю.", "Помилка ",
                MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (Regex.IsMatch(posada.Text, @"[^\u0400-\u04FF]"))
            {
                MessageBox.Show("Поле посада повинно містити тільки кирилицю.", "Помилка ",
                MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (Regex.IsMatch(education.Text, @"[^\u0400-\u04FF]"))
            {
                MessageBox.Show("Поле освіта повинно містити тільки кирилицю.", "Помилка ",
                MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (!Regex.IsMatch(phone.Text, @"^[0-9]+$") || phone.TextLength != 10 || phone.Text[0] != '0')
            {
                MessageBox.Show("Некоректний номер телефону.");
            }
            else if (login.TextLength < 6)
            {
                MessageBox.Show("Логін повинен містити мінімум 6 символів!");
            }
            else if (!Regex.IsMatch(login.Text, @"^[0-9a-zA-Z]+$"))
            {
                MessageBox.Show("Логін повинен містити тільки цифри та латинські букви!");
            }
            else if (password.TextLength < 6)
            {
                MessageBox.Show("Пароль повинен містити мінімум 6 символів!");
            }
            else if (!Regex.IsMatch(password.Text, @"^[0-9a-zA-Z]+$"))
            {
                MessageBox.Show("Пароль повинен містити тільки цифри та латинські букви!");
            }
            else
            {
                try
                {
                    using (var webClient = new MyWebClient())
                    {
                        var pars = new NameValueCollection();
                        pars.Add("registration", name.Text + '&' + surname.Text + '&' + lastname.Text + '&' + city.Text + '&' + date_age.Text + '&' + posada.Text + "&" + education.Text + '&' + phone.Text + "&" + login.Text + '&' + password.Text);
                        var response = webClient.UploadValues(Data.URL, pars);
                        string str = System.Text.Encoding.UTF8.GetString(response);
                        if (str == "Користувач з даним логіном вже існує.")
                        {
                            MessageBox.Show(str);
                        }
                        else
                        {
                            Data.Login = login.Text;
                            MessageBox.Show(str);
                            Menu form = new Menu();
                            form.FormClosed += new FormClosedEventHandler(delegate { Close(); });
                            form.Show();
                            this.Hide();
                        }
                    }
                }
                catch (WebException)
                {
                    MessageBox.Show("Сервер не відповідає, спробуйте пізніше.");
                }

            }
        }
        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Form1 form = new Form1();
            form.FormClosed += new FormClosedEventHandler(delegate { Close(); });
            form.Show();
            this.Hide();
        }

        private void registration_Load(object sender, EventArgs e)
        {
            
        }

        private void date_age_ValueChanged(object sender, EventArgs e)
        {

            if (this.date_age.Value > DateTime.Today)
            {
                this.date_age.Value = DateTime.Today;
            }
        }

        private void posada_TextChanged(object sender, EventArgs e)
        {

        }
    }   
}
