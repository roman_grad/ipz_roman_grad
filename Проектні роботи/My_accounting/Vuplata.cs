﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.Net;
using System.Collections.Specialized;

namespace My_accounting
{
    public partial class Vuplata : Form
    {
        public Vuplata()
        {
            InitializeComponent();
            try
            {
                using (var webClient = new MyWebClient())
                {
                    var pars = new NameValueCollection();
                    pars.Add("getPaymentData", Data.Login);
                    var response = webClient.UploadValues(Data.URL, pars);
                    string str = System.Text.Encoding.UTF8.GetString(response);
                    if(str != "")
                    {
                        var data = str.Split('&');
                        IndfCod.Text = data[0];
                        NumberPass.Text = data[1];
                        NumberCard.Text = data[2];
                    }
                }
            }
            catch (WebException)
            {
                MessageBox.Show("Сервер не відповідає, спробуйте пізніше.");
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (IndfCod.TextLength == 0 || NumberPass.TextLength == 0 || NumberCard.TextLength == 0 || CVV.TextLength == 0)
            {
                MessageBox.Show("Введіть данні для того щоб пройшла операція виплати грошей", "Помилка",
                MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (!Regex.IsMatch(IndfCod.Text, @"^[0-9]+$") || IndfCod.TextLength != 10)
            {
                MessageBox.Show("Некоректний ідентифікаційний код.", "Помилка",
                MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (!Regex.IsMatch(NumberPass.Text, @"^[0-9]+$"))
            {
                MessageBox.Show("Номер паспорта повинен містити тільки цифри.", "Помилка",
                MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (!Regex.IsMatch(NumberCard.Text, @"^[0-9]+$") || NumberCard.TextLength != 16)
            {
                MessageBox.Show("Некоректний номер карти.", "Помилка",
                MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (!Regex.IsMatch(CVV.Text, @"^[0-9]+$") || CVV.TextLength != 3)
            {
                MessageBox.Show("Некоректний номер карти.", "Помилка",
                MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (TickMarker.Checked == false)
            {
                MessageBox.Show("Ви забули поставити відмітку що погоджуєтесь з обробкою персональних даних", "Помилка ",
                MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                if(checkBox1.Checked == true)
                {
                    try
                    {
                        using (var webClient = new MyWebClient())
                        {
                            var pars = new NameValueCollection();
                            pars.Add("writePaymentData", Data.Login + '&' + IndfCod.Text + '&' + NumberPass.Text + '&' + NumberCard.Text);
                            var response = webClient.UploadValues(Data.URL, pars);
                            string str = System.Text.Encoding.UTF8.GetString(response);
                            MessageBox.Show(str);
                        }
                    }
                    catch (WebException)
                    {
                        MessageBox.Show("Сервер не відповідає, спробуйте пізніше.");
                    }
                }
                MessageBox.Show("Операція успішна");
            }
        }

        private void BackButton_Click(object sender, EventArgs e)
        {
            Menu form = new Menu();
            form.FormClosed += new FormClosedEventHandler(delegate { Close(); });
            form.Show();
            this.Hide();
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            if (this.dateTimePicker1.Value < DateTime.Today)
            {
                this.dateTimePicker1.Value = DateTime.Today;
            }
        }

        private void Vuplata_Load(object sender, EventArgs e)
        {

        }
    }   
}
